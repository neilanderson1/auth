<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\pdf

use mikehaertl\pdftk\Pdf;

class formController extends Controller
{
    //

public function __construct($pdfurl, $data) {
  $this->pdfurl = $pdfurl;

  $this->data   = $data;
	}
}

 public function fillpdf()
    {

        $pdf = new pdf('formfill.pdf', [
          'useExec' => true ]);
        $pdf->generateFdfFile('data.fdf');
        $pdf->fillForm(array(
           'product_type'=>'product_type',
            'trust' =>'trust_type',
            'address_details' => 'address',
            'personal_details' => 'personal',
            'corporate_details' => 'corporate',
            'directors' => 'directors',
            'company_beneficial_owners' => 'company',
            'other_entity_types' => 'other_entity_types',
            'account_setup_details' => 'account_setup_details',
            'other_services' => 'services',

            ));
            ->flatten();
            ->saveAs('filled.pdf');
            ->download('filled.pdf');
         // return view('form.blade.php');  
    	}
   }
