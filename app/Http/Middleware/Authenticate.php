<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
   //public function handle($request, Closure $next, $guard = null)
    class CheckUserSession 
    {
    public function handle($request, Closure $next)
    {

        //authenticate with api token
        $api_token = auth::user();
        Session::set("UserInfo", $api_token);
        $sessionId = \Session::getId();
        $request->session()->flash('status', 'you have 20 mins to complete the task');

        //if the Session is active lock the form and redirect the user
        if(\Session::get('locked') === true)
            return redirect('/locked');
        }
        //setting the session to a maximum time of 20 mins.
        elseif ( time() - Session::get('last_activity' >= 20) {
            return redirect('lockscreen');
        endif
        }
        //User info who makes changes/fills the form
        $request->attributes->add(["UserInfo" => Session::get("UserInfo")]);
        return $next($request);
    }
}
