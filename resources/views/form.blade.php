<?php

// Data to be written to the PDF form
$data = [
    'product_type' => 'Product',
    'trust'  => 'trust',
    'address_details' => 'address_details',
    'personal_details' => 'personal_details',
    'corporate_details' => 'corporate_details',
];

$pdf = new pdfForm('form.pdf', $data);

$pdf->flatten()
    ->save('outputs/form-filled.pdf')
    ->download();

exec("pdftk path/to/form.pdf fill_form $FDFfile output path/to/output.pdf flatten"); 

?>